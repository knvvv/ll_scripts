import json, glob, shutil

jobtypes = {
    'g16' : {
        'execution': 'rung {inpfile}'
    }
}

for i in range(30):
    shutil.copy2('h2o.gjf', './inpfiles/inpfile_%d.gjf' % i)

tasks = []
for i in range(1, 10):
    file = "./inpfiles/inpfile_%d.gjf" % i
    newtask = {
        'jobtype': 'g16',
        'inpfile': file,
        'nproc': 14,
        'logfile': file.replace('.gjf', '.log'),
    }
    tasks.append(newtask)

with open('docalc.dat', 'w') as f:
    f.write(json.dumps({'jobtypes': jobtypes, 
                        'tasks': tasks}))

tasks = []
for i in range(10, 31):
    file = "./inpfiles/inpfile_%d.gjf" % i
    newtask = {
        'jobtype': 'g16',
        'inpfile': file,
        'nproc': 4,
        'logfile': file.replace('.gjf', '.log'),
    }
    tasks.append(newtask)

with open('update_1.dat', 'w') as f:
    f.write(json.dumps({'command': 'priority',
                        'jobtypes': jobtypes, 
                        'tasks': tasks}))
