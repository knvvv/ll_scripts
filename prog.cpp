#include <mpi.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <list>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <stdexcept>
#include <chrono>
#include <thread>
#include <sys/types.h>
#include <signal.h>

#include "fmt/core.h"
#include "fmt/args.h"
#include "nlohmann/json.hpp"
#include <boost/type_index.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>

#include <pybind11/embed.h>

using nlohmann::json;
using boost::typeindex::type_id_with_cvr;
namespace py = pybind11;

const int ASAP_SIZE = 2;
const std::string UPDATEFILE = "./{jobname}_{idx}.dat";
const std::string DOCALCFILE = "./{jobname}.dat";

const std::string JOBTYPES_KW = "jobtypes";
const std::string JOBTYPE_KW = "jobtype";
const std::string TASKS_KW = "tasks";
const std::string EXEC_KW = "execution";

const std::string COMMAND_KW = "command";
const std::string ASAP_KW = "asap";
const std::string EXT_COMM = "extend";
const std::string PRIOR_COMM = "priority";
const std::string REPLACE_COMM = "replace";
const std::string ABORT_COMM = "abort";

const int ABORT_SIGNAL = -1488; // Must be negative

/*
 procs.append({
            'proc': subprocess.Popen(calc_file["command"], shell = True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL),
            'inpfile': calc_file,
            'logfile': calc_file["resfile"],
            'wd': tempwd,
            'nproc': curnproc,
            })

// std::vector<std::string> task_keys;
// task_keys.append(task_key)
// store.push_back(fmt::arg("inpfile", static_cast<std::string>(task_item["inpfile"])));
// auto keyvalue = boost::lexical_cast<std::string>(it.value());
// auto keyvalue = static_cast<std::string>(it.value());
// std::cout << "Keytype = " << type_id_with_cvr<decltype(keyname)>().pretty_name() << "\n";
// std::cout << "Valuetype = " << type_id_with_cvr<decltype(it.value())>().pretty_name() << "\n";

while(alive_threads > 1) {
    MPI_Status status;
    std::cout << "[MAIN] WAITING FOR DONE SIG" << std::endl;
    MPI_Recv(&done,1,MPI_C_BOOL,MPI_ANY_SOURCE,0,MPI_COMM_WORLD, &status);
    std::cout << "[MAIN] GOT A SIGNAL" << std::endl;
    if(execlines.empty()){
        size = 0;
        alive_threads -= 1;
        std::cout << "[MAIN] To go: " << alive_threads << std::endl;
        MPI_Send(&size, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
    } else {
        cur_execline = execlines.back();
        size = cur_execline.size();
        MPI_Send(&size, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
        MPI_Send(cur_execline.c_str(), size, MPI_CHAR, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
        execlines.pop_back();
    }
} */


void read_json(json& j, const std::string& filename)
{
    if(!boost::filesystem::exists(filename))
        throw std::runtime_error("File " + filename + " not found");
    
    std::ifstream i(filename);
    i >> j;
    if(!j.is_object())
        throw std::runtime_error("Must be an object");
    
    if(j.find(JOBTYPES_KW) == j.end())
        throw std::runtime_error("Jobtypes section not found");
    
    if(j.find(TASKS_KW) == j.end())
        throw std::runtime_error("Tasks section not found");
}

std::tuple<std::string, int> parse_docalc(const std::string& docalc_name, std::vector<std::string>& execlines)
{
    json j;
    // std::cout << "Reading " << docalc_name << std::endl;
    try {
        read_json(j, docalc_name);
    } 
    catch(const std::exception& exc) {
        fmt::print("[ERROR] File = '{}' : {}\n", docalc_name, exc.what());
        std::string command = "NO_COMMAND_PROVIDED";
        int asap_mode = 0;
        return std::tuple<std::string, int>(command, asap_mode);
    }

    // std::cout << "Res = " << j << std::endl;
    auto jobtypes = j[JOBTYPES_KW];
    auto tasks = j[TASKS_KW];
    for(auto& task_item : tasks)
    {
        auto jobtype_name = static_cast<std::string>(task_item[JOBTYPE_KW]);
        auto jobtype = jobtypes[jobtype_name];
        auto exec_template = static_cast<std::string>(jobtype[EXEC_KW]);

        fmt::dynamic_format_arg_store<fmt::format_context> store;
        for (auto it = task_item.begin(); it != task_item.end(); ++it)
        {
            const char* keyname = it.key().c_str();
            std::string keyvalue;
            if(it.value().is_string())
                keyvalue = it.value().get<std::string>();
            else
                keyvalue = boost::lexical_cast<std::string>(it.value());
            
            if(keyname != JOBTYPE_KW)
                store.push_back(fmt::arg(keyname, keyvalue));
        }
        std::string result = fmt::vformat(exec_template, store);
        execlines.push_back(result);
    }

    std::string command = "NO_COMMAND_PROVIDED";
    int asap_mode = 0;
    for (auto it = j.begin(); it != j.end(); ++it) {
        // std::cout << "Checking key " << it.key() << std::endl;
        if(it.key() == COMMAND_KW) {
            command = it.value().get<std::string>();
        } else if(it.key() == ASAP_KW) {
            asap_mode = it.value().get<int>();
        }
    }
    return std::tuple<std::string, int>(command, asap_mode);
}

bool update_queue(std::vector<std::string>& execlines, std::vector<std::string>& new_execlines, std::string& command)
{
    if (command == EXT_COMM) {
        execlines.insert(execlines.begin(), new_execlines.begin(), new_execlines.end());
    } else if (command == PRIOR_COMM) {
        execlines.insert(execlines.end(), new_execlines.begin(), new_execlines.end());
    } else if (command == REPLACE_COMM) {
        execlines = std::vector<std::string>(new_execlines);
    } else if (command == ABORT_COMM) {
        // Do nothing here
    } else {
        return false;
    }
    return true;
}

void pause_thread(int& size, const int& thread_rank)
{
    size = 0;
    fmt::print("Pausing the process of rank {}\n", thread_rank);
    MPI_Send(&size, 1, MPI_INT, thread_rank, 0, MPI_COMM_WORLD);
}

void pop_and_send(std::vector<std::string>& execlines, std::string& cur_execline, int& size, const int& thread_idx)
{
    cur_execline = execlines.back();
    size = cur_execline.size();
    fmt::print("Sending the line \"{}\" for calculation to rank {}\n", cur_execline, thread_idx);
    std::cout << "Sending " << cur_execline << std::endl;
    MPI_Send(&size, 1, MPI_INT, thread_idx, 0, MPI_COMM_WORLD);
    MPI_Send(cur_execline.c_str(), size, MPI_CHAR, thread_idx, 0, MPI_COMM_WORLD);
    execlines.pop_back();
}

void abort_calc(const int thread_idx) {
    fmt::print("[Driver] Sending abort signal to {}\n", thread_idx);
    int signal = ABORT_SIGNAL;
    MPI_Send(&signal, 1, MPI_INT, thread_idx, 0, MPI_COMM_WORLD);
}

int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);
    printf("Hello world from processor %s, rank %d out of %d processors\n",processor_name, world_rank, world_size);
    
    if(world_rank == 0){
        if(argc != 2)
            throw std::runtime_error("Unexpected number of arguments. 1 expected");
        auto job_name = std::string(argv[1]);
        auto docalc_name = fmt::format(DOCALCFILE, fmt::arg("jobname", job_name));;
        std::vector<std::string> execlines;
        std::vector<std::string> asaplines;
        while(!boost::filesystem::exists(docalc_name)) {
            fmt::print("[ATTENTION] Waiting for file '{}'\n", docalc_name);
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        parse_docalc(docalc_name, execlines);
        
        fmt::print("The file {} has been parsed. Exec lines are \n", docalc_name);
        for(const auto& line : execlines)
            std::cout << line << std::endl;
        
        std::string cur_execline;
        int n_asap_threads;
        if(world_size > ASAP_SIZE) {
            n_asap_threads = ASAP_SIZE;
        } else {
            fmt::print("[WARNING] ASAP_SIZE = {} but world_size = {}\nNot gonna create the asap pool\n", ASAP_SIZE, world_size);
            n_asap_threads = 0;
        }
        int size;

        for(int i = 1; i < n_asap_threads + 1;++i) {
            pause_thread(size, i);
        }

        for(int i = n_asap_threads + 1; i < world_size; ++i) {
            if(execlines.empty())
                pause_thread(size, i);
            else
                pop_and_send(execlines, cur_execline, size, i);
        }
        
        int update_docalc = 1, complete_recv = -1;
        bool done;
        MPI_Status status;
        MPI_Request request;
        while(true) {
            try {
                if (complete_recv != -1) { // Request must be initiated first
                    MPI_Test(&request, &complete_recv, &status);
                    // fmt::print("Running MPI_Test. Obtained complete_recv = {}\n", complete_recv);
                }

                if (complete_recv == 1) {
                    if (status.MPI_SOURCE <= n_asap_threads) {
                        if(!asaplines.empty()) {
                            pop_and_send(asaplines, cur_execline, size, status.MPI_SOURCE);
                        } else {
                            pause_thread(size, status.MPI_SOURCE);
                        }
                    } else {
                        if(!execlines.empty()) {
                            pop_and_send(execlines, cur_execline, size, status.MPI_SOURCE);
                        } else if(!asaplines.empty()) {
                            pop_and_send(asaplines, cur_execline, size, status.MPI_SOURCE);
                        } else {
                            pause_thread(size, status.MPI_SOURCE);
                        }
                    }
                }

                if ((complete_recv == -1) || (complete_recv == 1)) // Just getting started or just recieved a done sig
                {
                    MPI_Irecv(&done, 1, MPI_C_BOOL, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &request);
                    // fmt::print("Created Irecv request\n");
                    complete_recv = 0;
                }

                std::string updatefile_name = fmt::format(UPDATEFILE, fmt::arg("idx", update_docalc), fmt::arg("jobname", job_name));
                if (boost::filesystem::exists(updatefile_name))
                {
                    fmt::print("Parsing updatefile {}\n", updatefile_name);
                    std::vector<std::string> new_execlines;
                    
                    std::string command;
                    int asap_mode;
                    std::tie(command, asap_mode) = parse_docalc(updatefile_name, new_execlines);
                    // std::cout << "Parsed execlines:" << std::endl;
                    // for(const auto& task : new_execlines)
                    //     std::cout << "* " << task << std::endl;

                    if(asap_mode == 0) {
                        if(!update_queue(execlines, new_execlines, command))
                            fmt::print("[WARNING] Updatefile {} is discarded ({} was not recognezed as command)\n", updatefile_name, command);
                    } else if (asap_mode == 1) {
                        if(!update_queue(asaplines, new_execlines, command))
                            fmt::print("[WARNING] Updatefile {} is discarded ({} was not recognezed as command)\n", updatefile_name, command);
                    } else {
                        fmt::print("[WARNING] Updatefile {} is discarded (asap_mode is either 1 or 0, not {})\n", updatefile_name, asap_mode);
                    }
                    update_docalc += 1;

                    if (command == ABORT_COMM) {
                        execlines.clear();
                        asaplines.clear();
                        fmt::print("[Driver] execlines size = {}\n", execlines.size());
                        fmt::print("[Driver] asaplines size = {}\n", asaplines.size());
                        for(int i = 1; i < world_size; ++i) // Aborts both asap and common threads
                            abort_calc(i);
                        std::cout << std::endl;
                    }

                    std::cout << "Updated list of tasks:" << std::endl;
                    for(const auto& task : execlines)
                        std::cout << task << std::endl;
                    if(execlines.size() == 0)
                        std::cout << "EMPTY" << std::endl;

                    std::cout << "Updated ASAP-list:" << std::endl;
                    for(const auto& task : asaplines)
                        std::cout << task << std::endl;
                    if(asaplines.size() == 0)
                        std::cout << "EMPTY" << std::endl;
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            } 
            catch(const std::exception& exc) {
                fmt::print("[Driver] Exception occured : {}\n", exc.what());
                fmt::print("[Driver] Attempting to resume\n");
            }
        }
    } else {
        using namespace std::chrono_literals;
        using namespace py::literals;
        py::scoped_interpreter guard{};

        std::string myrank = boost::lexical_cast<std::string>(world_rank);

        auto locals = py::dict("myrank"_a=py::cast(myrank));
        py::exec(R"(
            message = f"Rank {myrank} : Hello, testing, 123\n"
        )",  py::globals(), locals);

        auto message = locals["message"].cast<std::string>();
        std::cout << message;

        auto py_subprocess = py::module_::import("subprocess");
        auto py_signal = py::module_::import("signal");
        auto py_os = py::module_::import("os");

        while(true) {
            try {
                int size;
                MPI_Recv(&size,1,MPI_INT,0,0,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                if((size == 0) || (size == ABORT_SIGNAL))
                {
                    std::this_thread::sleep_for(std::chrono::seconds(10));
                    bool done = true;
                    MPI_Send(&done, 1, MPI_C_BOOL, 0, 0, MPI_COMM_WORLD);
                    continue;
                }
                
                std::vector<char> exec_chars(size, '\0');
                MPI_Recv(&exec_chars[0], size, MPI_CHAR, 0, 0, MPI_COMM_WORLD,  MPI_STATUS_IGNORE);
                std::string execstring(exec_chars.begin(), exec_chars.end());
                // std::cout << "Rcved " << execstring << std::endl;
                execstring += " " + myrank; // Hack for thread index
                
                auto process = py_subprocess.attr("Popen")(py::cast(execstring), 
                                           "stdout"_a=py_subprocess.attr("PIPE"),
                                           "shell"_a=py::cast(true),
                                           "preexec_fn"_a=py_os.attr("setsid"));
                
                MPI_Request abort_request;
                MPI_Status abort_status;
                int abort_received = 0, recved_signal = 0;
                MPI_Irecv(&recved_signal, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &abort_request);

                while(process.attr("poll")().is(py::none()) && (recved_signal != ABORT_SIGNAL)) {
                    MPI_Test(&abort_request, &abort_received, &abort_status);
                    // fmt::print("[Thread {}] Waiting for subprocess\n", myrank);
                    std::this_thread::sleep_for(std::chrono::seconds(1));
                }
                if (recved_signal == ABORT_SIGNAL) {
                    fmt::print("[Thread {}] Gotta kill the process\n", myrank);
                    py_os.attr("killpg")(py_os.attr("getpgid")(process.attr("pid")), py_signal.attr("SIGTERM"));
                } else {
                    MPI_Cancel(&abort_request);
                }
                fmt::print("[Thread {}] Done\n", myrank);

                bool done = true;
                MPI_Send(&done, 1, MPI_C_BOOL, 0, 0, MPI_COMM_WORLD);
            }
            catch(const std::exception& exc) {
                fmt::print("[Thread {}] Exception occured : {}\n", myrank, exc.what());
                fmt::print("[Thread {}] Attempting to resume\n", myrank);
            }
        }
    }
    MPI_Finalize();
}
